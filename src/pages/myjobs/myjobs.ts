import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-myjobs',
  templateUrl: 'myjobs.html'
})
export class MyJobsPage {

  constructor(public navCtrl: NavController) {

  }

}
